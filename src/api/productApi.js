import request from './request';

export const getPrice = (
  data,
  successAction,
  failureAction,
) => async dispatch => {
  await dispatch(
    request({
      path: 'price',
      successAction,
      failureAction,
      opts: {
        data,
        method: 'POST',
      },
    }),
  );
  return Promise.resolve();
};