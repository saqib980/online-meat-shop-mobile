import axios from 'axios';
import {BASE_URL} from '../config/apiConfig';
import {fetchingApi, fetchedApi} from '../actions/configurationAction';
const baseURL = `${BASE_URL}/api/v1`;

const request = ({
  path,
  successAction,
  failureAction,
  opts = {},
}) => async dispatch => {
  await dispatch(fetchingApi());
  try {
    const res = await axios({
      url: `${baseURL}/${path}`,
      ...opts,
    });
    res.data.success === undefined
      ? await dispatch(successAction(res.data))
      : !!res.data.success
      ? await dispatch(successAction(res.data))
      : await dispatch(failureAction(error));
  } catch (error) {
    await dispatch(failureAction(error));
  }
  await dispatch(fetchedApi());
  return Promise.resolve();
};

export default request;
