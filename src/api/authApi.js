import request from './request';

export const getUser = (successAction, failureAction) => async dispatch => {
  await dispatch(
    request({
      path: 'me',
      successAction,
      failureAction,
      opts: {
        method: 'GET',
      },
    }),
  );
  return Promise.resolve();
};

export const signup = (
  data,
  successAction,
  failureAction,
) => async dispatch => {
  await dispatch(
    request({
      path: 'users',
      successAction,
      failureAction,
      opts: {
        data,
        method: 'POST',
      },
    }),
  );
  return Promise.resolve();
};

export const signin = (
  data,
  successAction,
  failureAction,
) => async dispatch => {
  await dispatch(
    request({
      path: 'login',
      successAction,
      failureAction,
      opts: {
        data,
        method: 'POST',
      },
    }),
  );
  return Promise.resolve();
};
export const updateProfileApi = (
  data,
  successAction,
  failureAction,
) => async dispatch => {
  await dispatch(
    request({
      path: 'profile',
      successAction,
      failureAction,
      opts: {
        data,
        method: 'POST',
      },
    }),
  );
  return Promise.resolve();
};
