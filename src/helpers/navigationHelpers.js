import {NavigationActions, StackActions} from 'react-navigation';
import {DrawerActions} from 'react-navigation-drawer';
let navigator;

export const resetStackAndNavTo = (route, navigation) => {
  const resetAction = StackActions.reset({
    index: 0,
    actions: [NavigationActions.navigate({routeName: route})],
  });
  navigation.dispatch(resetAction);
};

export const openDrawer = navigation => {
  navigation.dispatch(DrawerActions.openDrawer());
};

export const closeDrawer = navigation => {
  navigation.dispatch(DrawerActions.closeDrawer());
};

export const toggleDrawer = navigation => {
  navigation.dispatch(DrawerActions.toggleDrawer());
};

export const setTopLevelNavigator = navigatorRef => {
  navigator = navigatorRef;
};

export const navigate = (routeName, params) => {
  navigator.dispatch(
    NavigationActions.navigate({
      routeName,
      params,
    }),
  );
};
