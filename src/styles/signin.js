import {StyleSheet} from 'react-native';
import {COLORS} from './colors';
import {setBorderRadius} from './base';

export default StyleSheet.create({
  topHalf: {
    flex: 1,
    backgroundColor: COLORS.lightBlue,
  },
  bottomHalf: {
    flex: 1.5,
    backgroundColor: COLORS.darkBlue,
    borderTopRightRadius: 40,
    borderTopLeftRadius: 40,
    paddingVertical: 30,
    paddingHorizontal: 10,
  },
  formGroup: {
    paddingVertical: 10,
  },
  inputStyles: {
    color: COLORS.darkBlue,
    fontSize: 12,
    marginHorizontal: 5,
  },
  inputContainerStyles: {
    marginVertical: 5,
    backgroundColor: COLORS.white,
    ...setBorderRadius(4, 4, 4, 4),
  },
  rememberPass: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingLeft: 10,
  },
  rememberPassCheckBox: {
    paddingRight: 0,
  },
  whiteText: {
    color: COLORS.white,
  },
  termsText: {
    fontSize: 8,
    color: COLORS.grey,
  },
  emailIconStyle: {
    width: 22,
    height: 16,
  },
  passwordIconStyle: {
    marginLeft: 5,
    marginRight: 2,
    alignSelf: 'flex-end',
  },
});
