import {StyleSheet, Dimensions, Platform} from 'react-native';
import {heightPercentageToDP as hp} from 'react-native-responsive-screen';
import {COLORS} from './colors';

export const setBorderRadius = (tl = 0, tr = 0, bl = 0, br = 0) => ({
  borderTopRightRadius: tl,
  borderTopLeftRadius: tr,
  borderBottomLeftRadius: bl,
  borderBottomRightRadius: br,
});

export default StyleSheet.create({
  padBottom: {
    paddingBottom: hp(10),
  },
  container: {
    flex: 1,
    backgroundColor: COLORS.lightBlue,
  },
  errorMsg: {
    fontSize: 13,
    color: '#ff9696',
    marginLeft: 20,
  },
  basePad: {
    padding: 20,
  },
  indicatorContainer: {
    position: 'absolute',
    zIndex: 8,
    flex: 1,
    top: Dimensions.get('window').height * 0.5,
    alignSelf: 'center',
  },
  title: {
    color: COLORS.white,
    fontSize: 34,
    fontWeight: 'bold',
  },
  darkTitle: {
    color: COLORS.black,
    fontSize: 24,
    marginBottom: hp(1.2),
    fontWeight: 'bold',
  },
  subTitle: {
    fontSize: 14,
    color: COLORS.lightGrey,
    fontFamily: 'Avenir',
  },
  center: {
    alignSelf: 'center',
  },
  memoH1: {
    paddingHorizontal: 10,
    paddingVertical: 10,
    fontSize: 24,
    color: COLORS.white,
    fontWeight: 'bold',
    fontFamily: 'Avenir',
  },
  primary: {
    backgroundColor: COLORS.yellow,
  },
  primaryText: {
    backgroundColor: COLORS.yellow,
    color: COLORS.black,
    fontSize: 16,
    color: COLORS.black,
  },
  primaryBlue: {
    ...setBorderRadius(4, 4, 4, 4),
    backgroundColor: COLORS.blue,
  },
  primaryBlueText: {
    color: COLORS.white,
    fontSize: 16,
  },
  linkButton: {
    backgroundColor: 'transparent',
  },
  linkButtonText: {
    fontSize: 12,
    fontWeight: 'bold',
  },

  linkButtonContainer: {
    marginTop: hp(1.2),
  },
  logo: {
    width: 90,
    height: 90,
  },
  logoText: {
    fontWeight: 'bold',
    fontSize: 24,
    color: COLORS.navyBlue,
  },
  centerChild: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  row: {
    flexDirection: 'row',
  },
  menuStyle: {
    width: 28,
    height: 12,
  },
  bold: {
    fontWeight: 'bold',
  },
  statusCardBody: {
    flexDirection: 'row',
  },
  statusCardContainer: {
    flexDirection: 'row',
  },
  imageCardContainer: {
    borderTopLeftRadius: 14,
    borderTopRightRadius: 14,
    borderBottomLeftRadius: 14,
    borderBottomRightRadius: 14,
  },
  borderShadowLight: {
    shadowOpacity: 0.15,
    shadowRadius: 7,
    shadowOffset: {
      width: 0,
      height: 6,
    },
  },
  borderShadowDark: {
    shadowOpacity: 0.25,
    shadowRadius: 24,
    shadowOffset: {
      width: 0,
      height: 6,
    },
  },
  statusCard: {
    flexDirection: 'row',
    marginRight: hp(1.2),
    marginBottom: hp(2.4),
    ...Platform.select({
      ios: {
        shadowColor: 'rgba(0,0,0, .2)',
        shadowOffset: {height: 0, width: 0},
        shadowOpacity: 0, //default is 1
        shadowRadius: 0, //default is 1
      },
      android: {
        elevation: 0,
      },
    }),
  },
  ...setBorderRadius(14, 14, 14, 14),
});
