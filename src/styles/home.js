import {StyleSheet, Platform} from 'react-native';
import {COLORS} from './colors';
import {setBorderRadius} from './base';

export default (homeStyles = StyleSheet.create({
  homeContainer: {
    backgroundColor: COLORS.lightBlue,
  },
  cardLg: {
    backgroundColor: COLORS.white,
    ...setBorderRadius(7, 7, 7, 7),
    margin: 0,
    marginBottom: 10,
    borderWidth: 0,
    ...Platform.select({
      ios: {
        shadowColor: 'rgba(0,0,0, .2)',
        shadowOffset: {height: 0, width: 0},
        shadowOpacity: 0, //default is 1
        shadowRadius: 0, //default is 1
      },
      android: {
        elevation: 0,
      },
    }),
  },
  cardTitle: {
    fontSize: 16,
    marginBottom: 15,
  },
  cardSubTitle: {
    fontSize: 12,
    color: COLORS.sky,
  },
  cardBody: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  paymentDueLabel: {
    fontSize: 19,
    marginBottom: 10,
  },
  paymentDueText: {
    fontSize: 14,
    color: COLORS.sky,
  },
  creditCardStyles: {
    width: 26,
    height: 18,
    marginLeft: 10,
  },
  messageCardContainer: {
    flexDirection: 'row',
  },
  cardsBanner: {
    display: 'flex',
    flexDirection: 'row',
  },
  myTasks: {
    height: 45,
  },
  messageCard: {
    backgroundColor: COLORS.white,
    ...setBorderRadius(14, 14, 14, 14),
    margin: 0,
    marginBottom: 10,
    marginRight: 10,
    height: 100,
    borderWidth: 0,
    ...Platform.select({
      ios: {
        shadowColor: 'rgba(0,0,0, .2)',
        shadowOffset: {height: 0, width: 0},
        shadowOpacity: 0, //default is 1
        shadowRadius: 0, //default is 1
      },
      android: {
        elevation: 0,
      },
    }),
  },
  messageCardBody: {
    flexDirection: 'column',
  },
  messagesCardText: {
    fontSize: 48,
    lineHeight: 48,
  },
  newsCard: {
    backgroundColor: COLORS.white,
    ...setBorderRadius(14, 14, 14, 14),
    margin: 0,
    flex: 1.2,
    height: 100,
  },
  newsCardTitle: {
    fontSize: 18,
    marginBottom: 3,
  },
  newsCardSubTitle: {
    fontSize: 14,
    marginBottom: 3,
    color: COLORS.sky,
  },
  newsCardRegular: {
    marginBottom: 10,
    fontSize: 12,
    color: COLORS.sky,
  },
  newsCardContainer: {
    flex: 1.2,
    flexDirection: 'column',
  },
  backgroundVideo: {
    marginTop: 10,
    borderRadius: 12,
  },
}));
