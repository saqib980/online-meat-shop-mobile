import { StyleSheet, Dimensions } from 'react-native';
import { COLORS } from './colors';

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#235CA0",
    height: Dimensions.get('window').height
  },
  header: {
    flexDirection: "row",
    padding: 15,
    justifyContent: "space-between",
  },
  logo: {
    height: 50,
    width: 50
  },
  drawerText: {
    paddingHorizontal: 20,
    alignSelf: "center"
  },
  drawerTitle: {
    fontSize: 20,
    color: "white"
  },
  drawerSubTitle: {
    paddingHorizontal: 20,
    alignSelf: "center"
  },
  closeDrawer: {
    height: 18,
    width: 18,
    alignSelf: "center",
  },
  logoutBtn: {
    marginLeft: 17,
  },
  logoutBtnText: {
    fontSize: 15,
    fontWeight: 'normal',
    color: COLORS.white,
  },
});
