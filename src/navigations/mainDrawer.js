import React from 'react';
import {useDispatch} from 'react-redux';
import {
  Dimensions,
  ScrollView,
  Text,
  View,
  Image,
  TouchableOpacity,
} from 'react-native';
import SafeAreaView from 'react-native-safe-area-view';
import {createDrawerNavigator} from 'react-navigation-drawer';
import {createStackNavigator} from 'react-navigation-stack';
import {createSwitchNavigator} from 'react-navigation';
import {DrawerItems} from 'react-navigation-drawer';

import {logoutAction} from '../actions/authActions';

import {COLORS} from '../styles/colors';

import {closeDrawer} from '../helpers/navigationHelpers';
import drawerStyles from '../styles/drawer';
import generalStyles from '../styles/base';

//profile screens
import Home from '../screens/home/index';

const CustomDrawerContentComponent = props => {
  const {navigation} = props;
  const dispatch = useDispatch();
  const logout = () => {
    dispatch(logoutAction());
    props.navigation.navigate('Auth');
  };
  return (
    <ScrollView>
      <SafeAreaView
        style={drawerStyles.container}
        forceInset={{top: 'always', horizontal: 'never'}}>
        <View style={drawerStyles.header}>
          <View style={generalStyles.row}>
            <View style={drawerStyles.drawerText}>
              <Text style={drawerStyles.drawerTitle}>Online Meat Shop</Text>
            </View>
          </View>

          <TouchableOpacity onPress={() => closeDrawer(navigation)}>
            <Image
              source={require('../assets/Elements/SVG/close-icon.svg')}
              style={drawerStyles.closeDrawer}
            />
          </TouchableOpacity>
        </View>

        <DrawerItems {...props} />

        <View style={generalStyles.row}>
          <TouchableOpacity
            onPress={() => logout()}
            style={drawerStyles.logoutBtn}>
            <Text style={drawerStyles.logoutBtnText}>Log out</Text>
          </TouchableOpacity>
        </View>
      </SafeAreaView>
    </ScrollView>
  );
};

const mainDrawer = createDrawerNavigator(
  {
    Home: {screen: Home},
  },
  {
    drawerWidth: Dimensions.get('window').width,
    contentComponent: props => <CustomDrawerContentComponent {...props} />,
    contentOptions: {
      activeTintColor: 'white',
      inactiveTintColor: COLORS.white,
      itemsContainerStyle: {
        marginVertical: 10,
      },
      itemStyle: {
        marginVertical: 0,
      },
      labelStyle: {
        fontSize: 15,
        fontWeight: 'normal',
      },
      iconContainerStyle: {
        opacity: 1,
      },
    },
  },
);

export default createSwitchNavigator(
  {
    MainDrawer: {screen: mainDrawer},
  },
  {
    initialRouteName: 'MainDrawer',
    headerMode: 'none',
  },
);
