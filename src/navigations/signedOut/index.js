import {createStackNavigator} from 'react-navigation-stack';
import signUpScreen from '../../screens/signup/index';
import signInScreen from '../../screens/signin/index';

export default createStackNavigator(
  {
    Signin: {
      screen: signInScreen,
    },
    Signup: {
      screen: signUpScreen,
    },
  },
  {
    initialRouteName: 'Signin',
    headerMode: 'none',
  },
);
