import {createSwitchNavigator, createAppContainer} from 'react-navigation';
import Entry from '../screens/entry/index';
import signedOut from './signedOut/index';
import MainDrawer from './mainDrawer';

const switchNavigator = createSwitchNavigator(
  {
    Entry: {
      screen: Entry,
    },
    Auth: {
      screen: signedOut,
    },
    App: {
      screen: MainDrawer,
    },
  },
  {
    initialRouteName: 'Entry',
  },
);

export default createAppContainer(switchNavigator);
