import AsyncStorage from '@react-native-community/async-storage';

export const _storeData = async (key, value) => {
  try {
    await AsyncStorage.setItem(key, JSON.stringify(value));
  } catch (error) {
    // Error saving data
  }
};

export const _retrieveData = async item => {
  try {
    const value = await AsyncStorage.getItem(item);
    if (value !== null) {
      return JSON.parse(value);
    }
  } catch (error) {
    // Error retrieving data
  }
};

export const _removeItem = async item => {
  try {
    await AsyncStorage.removeItem(item);
  } catch (error) {
    // Error retrieving data
  }
};
