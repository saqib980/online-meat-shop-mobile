import * as types from './actionTypes';

export const getProductPriceSuccess = payload => {
  return dispatch => {
    dispatch({
      type: types.GET_PRODUCT_PRICE_SUCCESS,
      payload,
    });
  };
};

export const getProductPriceFailure = payload => {
  return {
    type: types.GET_PRODUCT_PRICE_FAILURE,
    payload,
  };
};