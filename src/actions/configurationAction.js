import * as types from './actionTypes';

export const fetchingApi = () => {
  return {
    type: types.FETCHING_API,
  };
};

export const fetchedApi = () => {
  return {
    type: types.FETCHED_API,
  };
};
