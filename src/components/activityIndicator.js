import React from 'react';
import {ActivityIndicator, View} from 'react-native';
import {useSelector} from 'react-redux';
import {isFetchingApi} from '../reducers/configuration';
import generalStyles from '../styles/base';

export default function Indicator() {
  const fetchingAPi = useSelector(isFetchingApi);
  return (
    <>
      {fetchingAPi && (
        <View style={generalStyles.indicatorContainer}>
          <ActivityIndicator size="large" color="#A9AEBE" />
        </View>
      )}
    </>
  );
}
