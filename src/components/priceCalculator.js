import React,{useState,useEffect} from 'react';
import {View,Text,TextInput,} from 'react-native';
import {Button,Card} from 'react-native-elements';
import generalStyles from '../styles/base';
export default function PriceCalculator({product}){
    const [amount,setAmount] = useState(1)
    const minAmount=1;
    const maxAmount=999;
    function handleText(text)
    {
        if(text !== "")
        {
            if(parseInt(text,10)!==0){
                setAmount(parseInt(text,10))
            }
            else{
                setAmount(1);
            }
        }
        else
        {
            setAmount(1)
        }
    }
    return (
        <>
        {
        !!product && <Card title={product.name} image={{uri: product.picture_url}}>

            <Text>Price: {product.price} per {product.unit} ({product.currency})</Text>
            <View style={{...generalStyles.row,alignItems:"center"}}>
                <Text style={{marginRight:20}}>Amount:</Text>
                <Button 
                onPress={()=>amount===minAmount||setAmount(amount-1)} 
                title='-'
                />
                <TextInput 
                value={amount.toString()}
                keyboardType='numeric'
                onChangeText={handleText}
                style={{width:50,textAlign:"center"}}
                maxLength={3}
                />
                <Button onPress={()=>amount===maxAmount||setAmount(amount+1)} title="+"></Button>
            </View>
            <Text>
            Total Price: {product.price*amount} ({product.currency})
            </Text>
            <Button title="Add to Cart"></Button>
        </Card>
        }
        </>
    )
}