import {applyMiddleware, createStore} from 'redux';
import thunkMiddleware from 'redux-thunk';
// import {composeWithDevTools} from 'remote-redux-devtools';
import rootReducer from '../reducers/rootReducer';
import preloadedState from '../reducers/initialState';

export const configureStore = () => {
  const middlewares = [thunkMiddleware];
  const middlewareEnhancer = applyMiddleware(...middlewares);
  const enhancers = [middlewareEnhancer];
  // const composedEnhancers = composeWithDevTools(...enhancers);
  const store = createStore(rootReducer, preloadedState, ...enhancers);

  return store;
};
