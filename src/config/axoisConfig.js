import axios from 'axios';
import {_retrieveData} from '../asyncStorage/index';

export const axoisConfiguration = () => {
  axios.interceptors.request.use(
    async function(options) {
      options.headers.Authorization = `Basic ${await _retrieveData(
        'Token',
      )}`;
      options.headers['Content-Type'] = 'application/json';
      return options;
    },
    function(error) {
      return Promise.reject(error);
    },
  );
};
