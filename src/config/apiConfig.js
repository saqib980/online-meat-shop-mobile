let baseUrl = 'http://10.0.2.2:3000';

baseUrl = baseUrl.endsWith('/')
  ? baseUrl.substring(0, baseUrl.length - 1)
  : baseUrl;

export const BASE_URL = baseUrl;
