import * as types from '../actions/actionTypes';
import initialState from './initialState';
import {_storeData, _removeItem} from '../asyncStorage/index';

export default function auth(state = initialState.auth, action) {
  switch (action.type) {
    case types.SIGNUP_SUCCESS:
      _storeData('Token', action.payload.token);
      return {
        ...state,
        isLoggedIn: true,
        user: action.payload.user,
        error: false,
      };
    case types.SIGNUP_FAILURE:
      _removeItem('Token');
      return {
        ...state,
        isLoggedIn: false,
        error: true,
      };
      case types.AUTHENTICATE_USER:
        return {
          ...state,
          user: action.payload,
          isLoggedIn: true,
          error: false,
        };
      case types.UN_AUTHENTICATE_USER:
        _removeItem('Token');
        return {
          ...state,
          isLoggedIn: false,
          error: true,
        };
    case types.SIGNIN_SUCCESS:
      _storeData('Token', action.payload.token);
      return {
        ...state,
        isLoggedIn: true,
        user: action.payload.user,
        error: false,
      };
    case types.SIGNIN_FAILURE:
      _removeItem('Token');
      return {
        ...state,
        isLoggedIn: false,
        error: true,
      };
    case types.LOGOUT:
      _removeItem('Token');
      return {
        ...state,
        isLoggedIn: false,
      };
    case types.UPDATE_PROFILE_SUCCESS:
      return {...state, error: false};
    case types.UPDATE_PROFILE_FAILURE:
      return {...state, error: true};
    default:
      return state;
  }
}

export const isLoggedIn = state => state.auth.isLoggedIn;
export const isError = state => state.auth.error;
