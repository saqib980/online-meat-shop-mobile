import * as types from '../actions/actionTypes';
import initialState from './initialState';
export default function product(state = initialState.product, action){
    switch(action.type)
    {
        case types.GET_PRODUCT_PRICE_SUCCESS:
        console.log(action.payload)    
        return {
                products: action.payload
            }
        case types.GET_PRODUCT_PRICE_FAILURE:
            return {
                error: "Can't Get Product Price"
            }
        default: 
            return state;
    }
}