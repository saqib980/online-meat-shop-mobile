import * as types from '../actions/actionTypes';
import initialState from './initialState';

export default function configuration(
  state = initialState.configuration,
  action,
) {
  switch (action.type) {
    case types.FETCHING_API:
      return {
        ...state,
        isFetchingApi: true,
      };
    case types.FETCHED_API:
      return {
        ...state,
        isFetchingApi: false,
      };
    default:
      return state;
  }
}

export const isFetchingApi = state => state.configuration.isFetchingApi;
