const initialState = {
  auth: {
    student: null,
    isLoggedIn: false,
    error: false,
  },
  product:
  {
    products: [],
  },
  configuration: {
    isFetchingApi: false,
  },
};

export default initialState;
