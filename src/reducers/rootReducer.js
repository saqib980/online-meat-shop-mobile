import {combineReducers} from 'redux';
import auth from './authReducer';
import configuration from './configuration';
import product from './productReducer';
const rootReducer = combineReducers({
  auth,
  product,
  configuration
});

export default rootReducer;
