import React, {useState, useEffect, useRef} from 'react';
import {connect} from 'react-redux';
import {View, SafeAreaView, TouchableOpacity, ScrollView} from 'react-native';
import {Button} from 'react-native-elements';
import generalStyles from '../../styles/base';
import {Text} from 'react-native-elements';
//api
import {getPrice} from '../../api/productApi';
//actions
import {getProductPriceSuccess,getProductPriceFailure} from '../../actions/productActions';
import PriceCalculator from '../../components/priceCalculator';

const Home = props => {
  const products = props.products;
  const [amount,setAmount]=useState(1);
  const [total,setTotal]=useState(0);
useEffect(()=>{
  if (products.length===0)
  {
    const data={user_id: props.user.id}
    props.getPrice(data);
  }
},[products])
  const renderedProducts=products.map((product)=>{return(          <PriceCalculator 
    product={product} 
    key={product.id}
    />)});
  return (
    <SafeAreaView style={[generalStyles.container]}>
      <ScrollView>
        <View>
          {renderedProducts}
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

const mapStateTopProps = state => {
  return {
   user: state.auth.user,
   products: state.product.products,
  };
};

const mapDispatchToProps = dispatch => ({
  getPrice: (data)=>dispatch(getPrice(data,getProductPriceSuccess,getProductPriceFailure))
});

export default connect(
  mapStateTopProps,
  mapDispatchToProps,
)(Home);
