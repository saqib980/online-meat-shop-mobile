import React, {useEffect} from 'react';
import {Text} from 'react-native-elements';
import {View} from 'react-native';
import {useSelector, useDispatch} from 'react-redux';
import {isLoggedIn, isError} from '../../reducers/authReducer';
import {getUser} from '../../api/authApi';
import {authenticateUser, unAuthenticateUser} from '../../actions/authActions';
import {_retrieveData} from '../../asyncStorage/index';

export default (Entry = ({navigation}) => {
  const dispatch = useDispatch();
  const loggedIn = useSelector(isLoggedIn);
  const isFetchApiError = useSelector(isError);
  useEffect(() => {
    if (loggedIn) {
      navigation.navigate('App');
    }
    if (isFetchApiError) {
      navigation.navigate('Auth');
    }
    async function asyncFunc() {
      const token = await _retrieveData('Token');
      const isRemember = await _retrieveData('isRemember');
      if (token && isRemember) {
        dispatch(getUser(authenticateUser, unAuthenticateUser));
      } else {
        navigation.navigate('Auth');
      }
    }
    if (!loggedIn) {
      asyncFunc();
    }
  }, [dispatch, loggedIn, isFetchApiError, navigation]);
  return (
      <View>
        <Text> loading...</Text>
      </View>
    
  );
});
