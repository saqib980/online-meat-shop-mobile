import React, {useState, useEffect, useCallback} from 'react';
import {useSelector, useDispatch} from 'react-redux';
import {Text, View, Linking} from 'react-native';
import {Input, Button, CheckBox} from 'react-native-elements';
import {signup} from '../../api/authApi';
import {signupSuccess, signupFailure} from '../../actions/authActions';
import {isLoggedIn, isError} from '../../reducers/authReducer';
import signInStyles from '../../styles/signin';
import generalStyles from '../../styles/base';
import ActivityIndicator from '../../components/activityIndicator';
import {_storeData} from '../../asyncStorage/index';
//svg icons
import PasswordIcon from '../../assets/Elements/SVG/icon_password.svg';
import MailIcon from '../../assets/Elements/SVG/icon_mail.svg';

import { BASE_URL } from '../../config/apiConfig';

export default (SignUp = ({navigation, ...props}) => 
{
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [confirmPassword, setConfirmPassword] = useState(''); 
  const [username, setUsername] = useState('');
  const dispatch = useDispatch();

  const loggedIn = useSelector(isLoggedIn);

  useEffect(() => {
    if (loggedIn) {
      navigation.navigate('App');
    }
  }, [loggedIn, navigation]);

  async function createUser() {
    const data = {
      user_session: {
        name: username,
        email,
        password,
      },
    };
    await dispatch(signup(data, signupSuccess, signupFailure));

  }

  return (
    <>
      <ActivityIndicator />
      <View style={[generalStyles.container]}>
        <View >
          <View style={signInStyles.formContainer}>
            <View style={signInStyles.formGroup}>
            <Input
                value={username}
                onChangeText={value => setUsername(value)}
                inputContainerStyle={signInStyles.inputContainerStyles}
                inputStyle={signInStyles.inputStyles}
                placeholder="Name"
                placeholderTextColor="#A9AEBE"
              />
              <Input
                value={email}
                onChangeText={value => { setEmail(value)}}
                placeholder="Email Address"
                inputContainerStyle={signInStyles.inputContainerStyles}
                inputStyle={signInStyles.inputStyles}
                autoCapitalize = 'none'
                placeholderTextColor="#A9AEBE"
                leftIcon={<MailIcon style={signInStyles.emailIconStyle} />}
              />
              <Input
                value={password}
                onChangeText={value => setPassword(value)}
                inputContainerStyle={signInStyles.inputContainerStyles}
                inputStyle={signInStyles.inputStyles}
                placeholder="Password"
                secureTextEntry={true}
                placeholderTextColor="#A9AEBE"
                leftIcon={
                  <PasswordIcon style={signInStyles.passwordIconStyle} />
                }
              />
              <Input
                value={confirmPassword}
                onChangeText={value => setConfirmPassword(value)}
                inputContainerStyle={signInStyles.inputContainerStyles}
                inputStyle={signInStyles.inputStyles}
                placeholder="Confirm Password"
                secureTextEntry={true}
                placeholderTextColor="#A9AEBE"
                leftIcon={
                  <PasswordIcon style={signInStyles.passwordIconStyle} />
                }
                />
            </View>

          </View>
        </View>

          <Button
            title="Sign Up"
            buttonStyle={generalStyles.primary}
            titleStyle={generalStyles.primaryText}
            onPress={async ()=> await createUser()}
          />
      </View>
    </>
  );
});