import React, {useState, useEffect, useCallback} from 'react';
import {useSelector, useDispatch} from 'react-redux';
import {Text, View, Linking} from 'react-native';
import {Input, Button, CheckBox} from 'react-native-elements';
import {signin} from '../../api/authApi';
import {signinSuccess, signinFailure} from '../../actions/authActions';
import {isLoggedIn, isError} from '../../reducers/authReducer';
import signInStyles from '../../styles/signin';
import generalStyles from '../../styles/base';
import ActivityIndicator from '../../components/activityIndicator';
import {_storeData} from '../../asyncStorage/index';
//svg icons
import PasswordIcon from '../../assets/Elements/SVG/icon_password.svg';
import MailIcon from '../../assets/Elements/SVG/icon_mail.svg';

import { BASE_URL } from '../../config/apiConfig';
import { log } from 'react-native-reanimated';

export default (SignIn = ({navigation, ...props}) => {
  const [signInFlag, setSignInFlag] = useState(false);
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [isRemember, setIsRemember] = useState(false);
  const dispatch = useDispatch();

  const loggedIn = useSelector(isLoggedIn);
  const isAuthError = useSelector(isError);

  useEffect(() => {
    if (loggedIn) {
      navigation.navigate('App');
    }
  }, [loggedIn, navigation]);

  async function authenticateUser() {
    _storeData('isRemember', isRemember);
    const data = {
      user_session: {
        email,
        password,
      },
    };
    await dispatch(signin(data, signinSuccess, signinFailure));
    if (loggedIn)
      !signInFlag && setSignInFlag(true);
  }

  return (
    <>
      <ActivityIndicator />
      <View style={[generalStyles.container]}>
        <View >
          {isAuthError && signInFlag && (
            <Text style={generalStyles.errorMsg}>
              email or password is incorrect
            </Text>
          )}

          <View style={signInStyles.formContainer}>
            <View style={signInStyles.formGroup}>
              <Input
                value={email}
                onChangeText={value => { setEmail(value)}}
                placeholder="Email Address"
                inputContainerStyle={signInStyles.inputContainerStyles}
                inputStyle={signInStyles.inputStyles}
                autoCapitalize = 'none'
                placeholderTextColor="#A9AEBE"
                leftIcon={<MailIcon style={signInStyles.emailIconStyle} />}
              />
              <Input
                value={password}
                onChangeText={value => setPassword(value)}
                inputContainerStyle={signInStyles.inputContainerStyles}
                inputStyle={signInStyles.inputStyles}
                placeholder="Password"
                secureTextEntry={true}
                placeholderTextColor="#A9AEBE"
                leftIcon={
                  <PasswordIcon style={signInStyles.passwordIconStyle} />
                }
              />
            </View>
          </View>

          <View style={signInStyles.rememberPass}>
            <Text style={signInStyles.whiteText}>Remember password</Text>

            <CheckBox
              containerStyle={signInStyles.rememberPassCheckBox}
              center
              checkedColor="white"
              uncheckedColor="white"
              checkedIcon="dot-circle-o"
              uncheckedIcon="circle-o"
              checked={isRemember}
              onPress={() => setIsRemember(!isRemember)}
            />
          </View>
          <Button
            title="Login"
            buttonStyle={generalStyles.primary}
            titleStyle={generalStyles.primaryText}
            onPress={authenticateUser}
          />

          <Button
            title="Create Account"
            containerStyle={generalStyles.linkButtonContainer}
            buttonStyle={generalStyles.linkButton}
            titleStyle={generalStyles.linkButtonText}
            onPress={() =>
              console.log(navigation.navigate('Signup'))
          }
          />
        </View>
      </View>
    </>
  );
});
