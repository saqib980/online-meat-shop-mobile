/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, {useEffect} from 'react';
import AppNavigator from './src/navigations/index';
import {axoisConfiguration} from './src/config/axoisConfig';
import {setTopLevelNavigator} from './src/helpers/navigationHelpers';

const App = () => {
  useEffect(() => {
    axoisConfiguration();
  }, []);

  return (
    <AppNavigator
      ref={navigatorRef => {
        setTopLevelNavigator(navigatorRef);
      }}
    />
  );
};

export default App;
